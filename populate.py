import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_lvl2.settings')

import django
django.setup()

from app_lvl2.models import User
from faker import Faker
fakegen = Faker()

def populate_user(N = 5):
    for entry in range(N):
        fakefirstname, fakelastname= fakegen.name().split()
        print(fakefirstname,fakelastname)
        fakeemail = fakegen.email()
        print(fakeemail)
        print(entry)
        usr = User.objects.get_or_create(first_name= fakefirstname, last_name= fakelastname, email= fakeemail)



if __name__ == "__main__":
    print("start populating ...")
    populate_user(10)
    print("end of populating.")