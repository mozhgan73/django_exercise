from django import forms
from app_lvl2.models import User


class UserInformationForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ("first_name", "last_name", "email")