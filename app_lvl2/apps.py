from django.apps import AppConfig


class AppLvl2Config(AppConfig):
    name = 'app_lvl2'
