from django.urls import path
from app_lvl2 import views

urlpatterns = [
    path('', views.show_users, name= 'show_users'),
]