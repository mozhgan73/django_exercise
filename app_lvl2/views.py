from django.shortcuts import render
from app_lvl2.models import User
from django.http import HttpResponse
from pprint import pprint as pp
from app_lvl2 import forms
# Create your views here.

def first_page(request):
    return HttpResponse("Please go to /user to see users information")

def show_users(request):
    User_Information = User.objects.all()
    user_dict = {'user_information': User_Information}
    # pp(user_dict)
    return render(request, 'applvl2/index.html', context= user_dict)

def insert_information(request):
    # form is a instance of UserInformationForm
    form = forms.UserInformationForm()

    if request.method == "POST":
        # form is an object of UserInformationForm that get its values from request.POST
        form = forms.UserInformationForm(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return show_users(request)
        else:
            print("ERROR FORM INVALIED")

    
    return render(request, 'applvl2/insert.html', {'form': form})